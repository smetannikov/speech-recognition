import yaml
import click
import pyaudio
import test

@click.command()
@click.option('--run', '-r', is_flag=True, help='')
@click.option('--mic-list', '-ml', is_flag=True, help='')
@click.option('--input-device', '-inDev', is_flag=True, help='')
@click.option('--default-device-change', '-ddc', is_flag=True, help='')


def main(run, mic_list, input_device, default_device_change):
    if run:
        test.run()

    if mic_list:
        p = pyaudio.PyAudio()
        for i in range(p.get_device_count()):
            print(i, p.get_device_info_by_index(i)['name'])

    if input_device:
        p = pyaudio.PyAudio()
        info = p.get_host_api_info_by_index(0)
        numdevices = info.get('deviceCount')
        for i in range(0, numdevices):
            if (p.get_device_info_by_host_api_device_index(0, i).get('maxInputChannels')) > 0:
                print("Input Device id ", i, " - ", p.get_device_info_by_host_api_device_index(0, i).get('name'))

    if default_device_change:
        with open('config.yaml') as f:
            data = yaml.safe_load(f)            
        p = pyaudio.PyAudio()
        info = p.get_host_api_info_by_index(0)
        numdevices = info.get('deviceCount')
        for i in range(0, numdevices):
            if (p.get_device_info_by_host_api_device_index(0, i).get('maxInputChannels')) > 0:
                print("Input Device id ", i, " - ", p.get_device_info_by_host_api_device_index(0, i).get('name'))
                data['INPUT_DEVICE_INDEX'] = i
                with open('config.yaml', 'w') as f:
                    yaml.dump(data, f)


if __name__ == '__main__':
    main()