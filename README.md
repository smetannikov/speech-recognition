# Необходимые библиотеки
**vosk** <br />
_Требования:_ <br />
- Python version: 3.5-3.9
- pip version: 20.3 and newer <br />
_Установка:_
- pip3 install vosk

**PyAudio** <br />
_Установка:_ <br />
- pip3 install PyAudio


**PyYAML** <br />
_Установка:_ <br />
- pip3 install PyYAML

**fuzzywuzzy** <br />
_Установка:_ <br />
- pip3 install fuzzywuzzy
