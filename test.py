def run():
    import os
    import json
    import pyaudio
    from fuzzywuzzy import fuzz
    from fuzzywuzzy import process
    from vosk import SetLogLevel
    from vosk import Model, KaldiRecognizer
    SetLogLevel(-1)


    import yaml
    with open('config.yaml') as f:
        config = yaml.safe_load(f)

    with open('comands.json') as f:
        comands = json.load(f)


    model = Model("model")
    rec = KaldiRecognizer(model, config['RATE'])
    p = pyaudio.PyAudio()
    stream = p.open(input_device_index = config['INPUT_DEVICE_INDEX'], format=pyaudio.paInt16, channels=config['CHANNELS'], rate=config['RATE'], input=True, frames_per_buffer=config['FRAMES_PER_BUFFER'])
    stream.start_stream()


    print('Я вас слушаю, можете диктовать команды')


    while True:
        data = stream.read(config['FRAMES_PER_BUFFER'])
        if len(data) == 0:
            break
        if rec.AcceptWaveform(data):
            res = json.loads(rec.Result())
            for i in comands:
                if (fuzz.partial_ratio(res['text'], i['text']) >= config['COMMAND_DIFFERENCE']) and (i["command_type"] == "RawText"):
                    print (i['text'])


    res = json.loads(rec.FinalResult())
    print (res['text'])
